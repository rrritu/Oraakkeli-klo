const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// data base's data structure 
const DataSchema = new Schema(
  {
    id: Number,
    message: String,
    count: {
      type: Number,
      default: 0
    },
    approved: {
      type: Boolean,
      default: false,
    },
    origin: {
      type: Boolean,
      default: false
    },
    reported: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("Data", DataSchema);