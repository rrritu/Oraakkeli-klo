//All routes in this project have /api subpath, this API router will prepend this path segment.

const express = require('express');
const apiRouter = express.Router();
const becomeRouter = require('./become.js');
const homeRouter = require('./home.js');

apiRouter.use('/become', becomeRouter);
apiRouter.use('/home', homeRouter);

module.exports = apiRouter;