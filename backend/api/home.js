//the CRUD operations for /api/become.
const mongoose = require('mongoose');
const express = require('express');
const homeRouter = express.Router();
const Data = require('../data');

mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);

// MongoDB database
const dbRoute =
  'mongodb+srv://mongodb.net/test?retryWrites=true&w=majority';

// connects back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });

let db = mongoose.connection;

db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// post method: fetch data in database
homeRouter.post('/getData', (req, res) => {
  const { question } = req.body;

  const love = /(rakka|raka)/gi;
  const alko = /(kalj|olut|ryyp|juo|viin|alkoh|pub|kapak)/gi;
  const angry = /(viha|huuta|nalkut|räyhä|tuska)/gi;
  const hello = /(moi|moro|hei|terve)/gi;

     
       if (question.match(hello)) {
         Data.countDocuments({ approved: true, message: {$regex: /(moi|moro|hei |terveh)/gi}}).exec(function(err, count) {
          let random = Math.floor(Math.random() * count);

          Data.findOne({ approved: true, message: {$regex: /(moi|moro|hei |terveh)/gi}}).skip(random).exec(function(err, result) {
            if (err) return res.json({ success: false, error: err });
            return res.json({ success: true, data: result.message });
          });
         })
        
      } else if (question.match(love)) {
        Data.countDocuments({ approved: true, message: {$regex: love}}).exec(function(err, count) {
          let random = Math.floor(Math.random() * count);

          Data.findOne({ approved: true, message: {$regex: love}}).skip(random).exec(function(err, result) {
            if (err) return res.json({ success: false, error: err });
            return res.json({ success: true, data: result.message });
          });
         })          
       
      } else if (question.match(alko)) {
        Data.countDocuments({ approved: true, message: {$regex: alko}}).exec(function(err, count) {
          let random = Math.floor(Math.random() * count);

          Data.findOne({ approved: true, message: {$regex: alko}}).skip(random).exec(function(err, result) {
            if (err) return res.json({ success: false, error: err });
            return res.json({ success: true, data: result.message });
          });
         })  
        
      } else if (question.match(angry)) {
        Data.countDocuments({ approved: true, message: {$regex: angry}}).exec(function(err, count) {
          let random = Math.floor(Math.random() * count);

          Data.findOne({ approved: true, message: {$regex: angry}}).skip(random).exec(function(err, result) {
            if (err) return res.json({ success: false, error: err });
            return res.json({ success: true, data: result.message });
          });
         })  
        
      } else {
        Data.countDocuments({ approved: true }).exec(function(err, count) {
          let random = Math.floor(Math.random() * count);

          Data.findOne({ approved: true }).skip(random).exec(function(err, result) {
            if (err) return res.json({ success: false, error: err });
            return res.json({ success: true, data: result.message });
          });
         })  
      };

});

module.exports = homeRouter;