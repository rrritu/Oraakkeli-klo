//the CRUD operations for /api/become.
const mongoose = require('mongoose');
const express = require('express');
const becomeRouter = express.Router();
const Data = require('../data');

mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);

// MongoDB database
const dbRoute =
  'mongodb+srv://mongodb.net/test?retryWrites=true&w=majority';

// connects back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });

let db = mongoose.connection;

db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// get method: fetch all available data in database send by user
becomeRouter.get('/getData', (req, res) => {
  Data.find({origin: false, reported: false}).exec(function(err, data) {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data });
      });
  });

// get method: fetch all available data in database send by user, sorted by createdAt timestamp, DESC order
  becomeRouter.get('/getDataDesc', (req, res) => {
    Data.find({origin: false, reported: false}).sort({createdAt: -1}).exec(function(err, data) {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data: data });
        });
    });

// get method: fetch all available data in database send by user, sorted by count, DESC order
    becomeRouter.get('/getDataCount', (req, res) => {
      Data.find({origin: false, reported: false}).sort({count: -1}).exec(function(err, data) {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data: data });
        });
    });
  
// update method: overwrite existing data in database
  becomeRouter.post('/updateData', (req, res) => {
    const { id, update } = req.body;
    Data.findByIdAndUpdate(id, update, (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true });
    });
  });
  
  // delete method: remove existing data in database
  becomeRouter.delete('/deleteData', (req, res) => {
    const { id } = req.body;
    Data.findByIdAndRemove(id, (err) => {
      if (err) return res.send(err);
      return res.json({ success: true });
    });
  });
  
  // create method: add new data in database
  becomeRouter.post('/putData', (req, res) => {
    let data = new Data();
  
    const { id, message } = req.body;
  
    if ((!id && id !== 0) || !message) {
      return res.json({
        success: false,
        error: 'INVALID INPUTS',
      });
    }
    data.message = message;
    data.id = id;
    data.save((err) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true });
    });
  });
  

module.exports = becomeRouter;