import React from 'react';

import Header from './Header/Header';
import Footer from './Footer/Footer';
import { BrowserRouter as Router, Route } from 'react-router-dom';


import Home from './Home/Home';

import Faq from './Faq/Faq';

import Become from './Become/Become';

import '../styles/styles.css';

class App extends React.Component {
    render() {
        return(
          <Router>
            <div>
                <Header />
                <main>
                <Route exact path="/" component={Home}/>
                <Route path="/faq" component={Faq}/>
                <Route path="/become" component={Become}/>
                </main>
                <Footer />
            </div>
          </Router>
        );
    }
}

export default App;
