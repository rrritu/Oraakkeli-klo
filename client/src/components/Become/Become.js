import React from 'react';
import axios from 'axios';
import AnswerList from './AnswerList';
import SortBy from './SortBy';

let url = 'http://' + window.location.host + '/api/become';


class Become extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      data: [],
      message: null,
      inputVal: null,
      url: `${url}/getDataDesc`,
    };

    this.sorted = this.sorted.bind(this);
    this.handleUpVote = this.handleUpVote.bind(this);
    this.handleDownVote = this.handleDownVote.bind(this);
    this.handleReport = this.handleReport.bind(this);
    this.getDataFromDb = this.getDataFromDb.bind(this);
  
  }

// render our database
componentDidMount() {
  this.getDataFromDb();  
}

componentDidUpdate(prevProps, prevState) {
  if (prevState.url !== this.state.url) {
    this.getDataFromDb();
  }
}

// get method that uses backend api to
// fetch data from our database
getDataFromDb = () => {
  fetch(this.state.url)
    .then((data) => data.json())
    .then((res) => this.setState({ data: res.data }));
}

// put method that uses backend api
// to create new query into our database
putDataToDB = (message) => {
  let currentIds = this.state.data.map((data) => data.id);
  let idToBeAdded = 0;
  while (currentIds.includes(idToBeAdded)) {
    ++idToBeAdded;
  }

  axios.post(`${url}/putData`, {
    id: idToBeAdded,
    message: message,
  })
  .then(() => this.getDataFromDb());
}

// handle submit button
handleSubmit(event){
  event.preventDefault();
  event.target.reset();
  this.setState({ inputVal: this.state.message });
}

// handle sorting option
sorted(sortByOption) {
  if (sortByOption ==='new') {
    this.setState({ url: `${url}/getDataDesc` });
  }
  if (sortByOption === 'old') {
    this.setState({ url: `${url}/getData` });
  }
  if (sortByOption ==='rating') {
    this.setState({ url: `${url}/getDataCount` });
  }
}

// handle upvote button
handleUpVote(messageId) {
  let objIdToUpdate = null;
  parseInt(messageId);
  this.state.data.forEach((dat) => {
      if (dat.id == messageId) {
          objIdToUpdate = dat._id;
      }
  });

  let updatedDat = null;
  this.state.data.forEach((dat) => {
      if (dat.id == messageId) {
          updatedDat = dat.count + 1;
      }
  });

  // check if answer reach approved number of votes
  if (updatedDat > 5) {
    axios.post(`${url}/updateData`, {
    id: objIdToUpdate,
    update: { count: updatedDat, approved: true },
    })
    .then(() => this.getDataFromDb());
  } else {
    axios.post(`${url}/updateData`, {
      id: objIdToUpdate,
      update: { count: updatedDat },
    })
    .then(() => this.getDataFromDb());
  }
}

//handle downvote button
handleDownVote = (messageId) => {
  let objIdToUpdate = null;
  parseInt(messageId);
  this.state.data.forEach((dat) => {
      if (dat.id == messageId) {
          objIdToUpdate = dat._id;
      }
  });

  let updatedDat = null;
  this.state.data.forEach((dat) => {
      if (dat.id == messageId) {
          updatedDat = dat.count - 1;
      }
  });

  // check if answer has no approved number of votes
  if (updatedDat < -3) {
      axios.delete(`${url}/deleteData`, {
          data: {
              id: objIdToUpdate,
          },
      })
      .then(() => this.getDataFromDb());
  } else if (updatedDat < 6) {
    axios.post(`${url}/updateData`, {
    id: objIdToUpdate,
    update: { count: updatedDat, approved: false },
    })
    .then(() => this.getDataFromDb());
  } else {
    axios.post(`${url}/updateData`, {
    id: objIdToUpdate,
    update: { count: updatedDat },
    })
    .then(() => this.getDataFromDb());
  }
}

// handle report button
handleReport = (messageId) => {
  let objIdToUpdate = null;
  parseInt(messageId);
  this.state.data.forEach((dat) => {
    if (dat.id == messageId) {
      objIdToUpdate = dat._id;
    }
  });

  axios.post(`${url}/updateData`, {
    id: objIdToUpdate,
    update: { reported: true },
  })
  .then(() => this.getDataFromDb());
}


render() {
  let confirm = '';
    if (this.state.inputVal) {
      confirm = <div>Ehdotus vastaanotettu!</div>
    }
  
  return (
      <div className="content">
          <h2>Haluatko jakaa viisauttasi Oraakkelin tavoin?</h2>
          <h2>Voit lähettää vastausehdotuksesi muiden käyttäjien arvioitavaksi:</h2><br/>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <input
              type="text"                    
              onChange={(event) => this.setState({ message: event.target.value })}                    
              placeholder="Jaa viisauttasi"/>
              <button type="submit" name="submit" onClick={() => this.putDataToDB(this.state.message)}>Lähetä</button>
          </form><br />
          {confirm}
          <h4>Lähetetyt ehdotukset näkyvät listassa. Parhaimmat ehdotukset pääsevät osaksi Oraakkelin vastauksia.</h4><br/>
          <div className="answer-container">
          <SortBy data={this.state.data} sorted={this.sorted} />
          <AnswerList data={this.state.data} voteUp={this.handleUpVote} 
                      voteDown={this.handleDownVote} report={this.handleReport} />
          </div>
      </div>
  );
}
}
export default Become;


