import React from 'react';

class Answewr extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showReport: false, 
            showButton: true,
            noUp: true,
            noDown: true,
            voted: false
        };

        this.handleUpClick = this.handleUpClick.bind(this);
        this.handleDownClick = this.handleDownClick.bind(this);
        this.handleReport = this.handleReport.bind(this);
        this.handleReportClick = this.handleReportClick.bind(this);

    }

    

    handleUpClick() {
        if (this.state.voted === false) {
        this.setState(state => ({
            noUp: !state.noUp,
            noDown: true,
            voted: true,
        }));
        this.props.voteUp(this.props.id);
        }
    }

    handleDownClick() {
        if (this.state.voted === false) {
        this.setState(state => ({
            noDown: !state.noDown,
            noUp: true,
            voted: true,
        }));
        this.props.voteDown(this.props.id);
        }
    }
    
    handleReportClick() {
        this.setState(state => ({
            showReport: !state.showReport,
            showButton: !state.showButton
        }));
    }

    handleReport = () => this.props.report(this.props.id);
    

  render() {
      return (
        <div className="row" >
            <div className="row-item" style={{ width: '100%' }}>  
                <p>{this.props.message}</p>
            </div>
            <div className="row-item" style={{ textAlign: 'right' }}>
                {this.state.noUp
                ? <button className="material-icons" 
                onClick={this.handleUpClick}>thumb_up_alt</button>
                : <button className="material-icons" 
                style={{ color: 'darkgreen'}}>thumb_up_alt</button>
                }
                {this.state.noDown
                ? <button className="material-icons" 
                onClick={this.handleDownClick}>thumb_down_alt</button>
                : <button className="material-icons"
                style={{ color: 'darkred' }}>thumb_down_alt</button>
                }
                
            </div>
            <div className="row-item">
                <p style={{ textAlign: 'center' }}>{this.props.count}</p>
            </div>
            <div className="row-item" >
                {this.state.showButton 
                ? <button className="material-icons"
                style={{ color: 'darkred', float: 'right', marginRight: '10px' }}
                onClick={this.handleReportClick}>report</button> 
                : <div className="report">
                    Haluatko ilmiantaa ehdotuksen?<br/>
                    <button onClick={this.handleReport}>Kyllä</button><button onClick={this.handleReportClick}>Ei</button>
                </div>
                }
            </div>
        </div>
      );
  }
      
}

export default Answewr;