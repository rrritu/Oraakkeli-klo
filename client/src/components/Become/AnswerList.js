import React from 'react';
import Answer from './Answer';

class AnswerList extends React.Component {
  render() {
      const data = this.props.data;
      return (
          <ul className="answers">
            {data.length <= 0
              ? 'Ryhdy ensimmäiseksi Oraakkeliksi!'
              : data.map((dat) => {
                return <Answer key={dat.id} message={dat.message} 
                              id={dat.id} 
                              count={dat.count} 
                              voteUp={this.props.voteUp} 
                              voteDown={this.props.voteDown}
                              report={this.props.report} />
                    })}
                </ul>
      );
  }
      
}
export default AnswerList;