import React from 'react';

class SortBy extends React.Component {
    constructor(props) {
        super(props);

        this.sortByOptions = {
            'Uusimmat': 'new',
            'Vanhimmat': 'old',
            'Parhaat': 'rating'
        };
    }


handleSortByChange(sortByOption) {
    this.props.sorted(sortByOption);
}

renderSortByOptions() {
    return Object.keys(this.sortByOptions).map(sortByOption => {
        let sortByOptionValue = this.sortByOptions[sortByOption];
        return <a key={sortByOptionValue} 
        onClick={this.handleSortByChange.bind(this, sortByOptionValue)}>{sortByOption}</a>;
    });
}

    render() {
        return (
            <div className="smallHeader">
                    <h4 className="smallHeader-item" style={{ textAlign: 'left' }}>Vastausehdotukset</h4>
                    <div className="smallHeader-item" style={{ width: '100%' }}></div>
                    <h4 className="smallHeader-item" style={{ textAlign: 'center' }} >Ääniä yhteensä</h4>
                    
                    <div className="smallHeader-item" style={{ textAlign: 'center' }}>
                        <div className="dropdown" style={{ textAlign: 'center' }}>
                        <div className="dropbtn" style={{ textAlign: 'center' }}>Järjestä <i className="material-icons">arrow_drop_down</i></div> 
                        <div className="dropdown-content">
                             {this.renderSortByOptions()}
                        </div>
                        </div>
                    </div>
                </div>
        );
    }
}

export default SortBy;