import React from 'react';
import { Link } from 'react-scroll';

class NavBar extends React.Component {
    render() {
        return (
            <ul className="sidebox">
                <li className="nav-item">
                    <Link to="section1" spy={true} smooth={true} offset={-125} activeClass="current">Mikä on Oraakkeli?</Link> </li>
                <li className="nav-item">
                    <Link to="section2" spy={true} smooth={true} offset={-125} activeClass="current">Mitä kysymyksiä voin kysyä Oraakkelilta?</Link> </li>
                <li className="nav-item">
                    <Link to="section3" spy={true} smooth={true} offset={-125} activeClass="current">Tallennetaanko kysymykseni?</Link> </li>
                <li className="nav-item">
                    <Link to="section4" spy={true} smooth={true} offset={-125} activeClass="current">Miten Oraakkeli osaa vastata kysymyksiini?</Link> </li>
                <li className="nav-item">
                <Link to="section5" spy={true} smooth={true} offset={-125} activeClass="current">Mitä tarkoittaa "Ryhdy Oraakkeliksi"?</Link></li>
            </ul>
        );
    }
}

export default NavBar;