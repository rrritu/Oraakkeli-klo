import React from "react";

export default function Section({ title, subtitle, id }) {
  return (
    <div className={"section"} id={id}>
      <div className="section-content">
        <h2>{title}</h2>
        {subtitle}
      </div>
    </div>
  );
}