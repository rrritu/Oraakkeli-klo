import React from 'react';

import items from './data';
import NavBar from './NavBar';
import Section from './Section';

class Faq extends React.Component {
    render() {
        return(
            <div className="faq-content">
                <NavBar />
                <div className="faq">
                    <h1>"Tunne itsesi"</h1>
                    <h6>- Delfoin oraakkeli</h6><br />
                    {
                        items.map((item) => {
                            return (
                                <div key={item.title}>
                                <Section title={item.title} subtitle={item.text} id={item.id} />
                                <br/></div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}  

export default Faq;