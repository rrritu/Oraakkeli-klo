import React from 'react';


const items = [
    {
        title: 'Mikä on Oraakkeli?',
        id: 'section1',
        text: <div><p>Oraakkeli on internetin ihmeelliseen maailmaan soluttautunut papitar, joka auttaa elämän 
        kiperien kysymysten äärellä. Voit kysyä vapaamuotoisen kysymyksen ja jumalainen Oraakkeli kertoo 
        sinulle varmasti oikean vastauksen. <br/><br/>
        
        Suoraan <a style={{ cursor: 'pointer', textDecoration: 'underline' }} href="https://fi.wikipedia.org/wiki/Oraakkeli">Wikipediasta</a> lainaten,
        </p> <blockquote>Oraakkeli (lat. oraculum) tarkoittaa henkilöä, joka välittää 
        tulevaisuutta koskevia viestejä jumalalta tai muilta yliluonnollisilta toimijoilta. Alun perin 
        latinankielinen sana tarkoitti nimenomaan jumalan antamaa vastausta esitettyyn kysymykseen. 
        Oraakkelilla tarkoitetaan myös paikkaa, jossa vastauksia annetaan. Vastaukset kysymyksiin välitettiin 
        tyypillisesti esimerkiksi meedion tai pappien kautta.</blockquote> 
        
        <p>Kuuluisin antiikin Kreikan oraakkeleista 
        oli <a style={{ cursor: 'pointer', textDecoration: 'underline' }} href="https://fi.wikipedia.org/wiki/Delfoin_oraakkeli">Delfoin oraakkeli</a> 
        , joka on myös tämän sivuston inspiraation 
        lähde. Delfoin oraakkeli oli Kreikan Delfoissa toiminut koko antiikin maailman tunnetuin ennustusinstituutio. 
        Oraakkeli toimi Apollonin temppelissä, missä hän istui kolmijalalla ja vastasi hänelle 
        esitettyihin kysymyksiin hurmostilan voimin.</p></div>
    },
    {
        title: 'Mitä kysymyksiä voin kysyä Oraakkelilta?',
        id: 'section2',
        text: <p>Parhaiten Oraakkeli osaa vastata mielipidekysymyksiin, joihin on useita mahdollisia vastauksia. 
        Voit esimerkiksi kysyä kysymyksiä omasta elämästäsi tai läheisistä ihmisistäsi. <br/><br/>

        Kysymys kannattaa aloittaa kysymyssanalla kuten milloin, missä, miltä, kuka, kenet, miksi, mikä, kuinka tms.
        "Mitä" sanaa kannattaa kuitenkin välttää, sillä sitä on vaikeampi tulkita. Tietokilpailukysymykset
        eivät filosofista Oraakkelia kiinnosta.</p>
    },
    {
        title: 'Tallennetaanko kysymykseni?',
        id: 'section3',
        text: <p>Oraakkeli ei tallenna kysymyksiäsi minnekään. Niinpä voit huoletta kysellä Oraakkelilta nolompiakin
        kysymyksiä. Oraakkeli toimii myös täysin konevoimin, joten kukaan ihminenkään ei lähettämiäsi 
        kysymyksiä tule lukemaan.</p>
    },
    {
        title: 'Miten Oraakkeli osaa vastata kysymyksiini?',
        id: 'section4',
        text: <p>Tarkkaa Oraakkelin toimintaa ei paljasteta, mutta toiminta on toteutettu hyvinkin yksinkertaisen 
        koodirivin avulla. Yksinkertaisuutensa takia parempiakin versioita samankaltaisista ohjelmistoista 
        varmasti löytyy, mutta kyllä se asiansa ajaa.</p>
    },
    {
        title: 'Mitä tarkoittaa "Ryhdy Oraakkeliksi"?',
        id: 'section5',
        text: <p>"Ryhdy Oraakkeliksi" osio on pieni lisä tälle sivustolle, jonka avulla kuka tahansa käyttäjä voi
        jakaa viisauttaan Oraakkelin tavoin. Idea on hyvinkin simppeli: kirjoita lausahdus joka sopisi mielestäsi 
        viisaan Oraakkelin sananparreksi ja lähetä se muiden vastausehdotusten sekaan. <br/><br/> 
        
        Lähetettyjä vastausehdotuksia voi kuka tahansa käyttäjä äänestää mielipiteensä mukaisesti. Kun 
        vastausehdotus on saanut tarpeeksi ääniä, se pääsee mukaan Oraakkelin vastauksien listalle. Vastaus voi myös 
        myöhemmin tippua pois listalta, mikäli se kerää tarpeeksi miinusääniä myöhemmin. <br/><br/>
        
        Lähetetty vastausehdotus voidaan myös äänestää ehdotuksista kokonaan pois tai se voidaan ilmiantaa, 
        joten vastausehdotusta kannattaa miettiä ennen lähettämistä. Ilmiannetut ehdotukset poistuvat 
        listalta välittömästi ja ylläpito päättää palautetaanko se takaisin listalle vai ei. <br/><br/>
        
        Tarkkoja äänimääriä näille rajoille ei paljasteta, sillä se 
        saattaa vaihtua sivuston suosion mukaan.</p>
    }
]

export default items;