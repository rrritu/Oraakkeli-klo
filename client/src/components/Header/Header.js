import React from 'react';
import headerImage from '../../img/logo4.png';

import { Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';


const Header = () => (
  <header>
    <div className="container">
        <div className="justify-start">
            <Link to="/"><img className="logo" src={headerImage} alt="logo"/></Link>
        </div>
        <div className="navbar">
              <NavLink to="/faq" activeClassName="current">Mikä ihmeen Oraakkeli?</NavLink>
              <NavLink to="/become" activeClassName="current">Ryhdy Oraakkeliksi</NavLink>
        </div>
    </div>
  </header>
);

export default Header;
