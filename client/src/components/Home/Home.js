import React, { Component } from 'react';
import 'whatwg-fetch';
import mainPic from '../../img/logo3.png';
import axios from 'axios';

let url = window.location;

class Ask extends Component {
    constructor(props) {
      super(props);
      this.state = {
        ask: '',
        inputVal: '',
        randAnswer: null
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);

    }


    handleChange(event) {
      const {value} = event.target;
      this.setState({ inputVal: value });
    }

    handleSubmit(event) {
      event.preventDefault();
      this.setState({ ask: this.state.inputVal, inputVal: '' });
      this.getRandomElement();
    }

    getRandomElement = () => {
      let question = this.state.inputVal;
      const hello = /(moi|moro|hei|terve)/gi;
      const being = /(olen|olin|oon|olet|olit|oot)/gi;

      // logic to filter some answers
      if (question === this.state.ask) {
        this.setState({ randAnswer: 'Älä jankkaa.'});
      } else if(question.split(' ').length < 2 && !question.match(hello)) {
        this.setState({ randAnswer: 'Typoile vähemmän, viestität enemmän.' });
      } else if (question.match(being)) {
        const match = /(olen|olin|oon|olet|olit|oot) (\S+(\s\S+)*)/gi.exec(question);
        if (match === null && question.match(/(olet|olit|oot)/gi)) {
          const answer = 'Minä olen Oraakkeli.';
          this.setState({ randAnswer: answer });
        } else if (match === null) {
          axios.post(`${url}api/home/getData`, {
            question: question
          })
          .then(response => {
            this.setState({ randAnswer: response.data.data });
          }, error => {
            console.log(error);
          });
        } else {
          console.log(match);
          let answer = 'Taisit tulla tänne koska olet  ' + match[2] + '.';
          if (answer.match(/minä/gi)) {
            answer = answer.replace(/minä/gi, "sinä");
          }
          this.setState({ randAnswer: answer });
        }
      } else {
        axios.post(`${url}api/home/getData`, {
          question: question
        })
        .then(response => {
          this.setState({ randAnswer: response.data.data });
        }, error => {
          console.log(error);
        });
      }
    }



    render() {
      const { inputVal, ask, randAnswer } = this.state;
      let header = '';
      
      if (ask) {
        header = <div><h3>Kysyit: {ask}</h3><h3>Oraakkeli vastaa: {randAnswer}</h3></div>;
      } else {
        header = <h2>Löydä vastaukset mieltäsi askarruttaviin kysymyksiin</h2>;
      }

      return (
        <div>
          <div className="content">
            {header}
            <img id="mainpic" src={mainPic} alt="kuva"/>
            <form onSubmit={this.handleSubmit}>
              <input type="text" onChange={this.handleChange} value={inputVal} placeholder="Kirjoita kysymyksesi tähän" />
              <button type="submit" name="submit" >Kysy</button>
            </form>
          </div>
      </div>
      )
    }
}


export default Ask;
